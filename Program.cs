using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using MilkTeaStoreApp.Areas.Identity.Data;
using MilkTeaStoreApp.Config;
using MilkTeaStoreApp.Data;
using MilkTeaStoreApp.Services;

var builder = WebApplication.CreateBuilder(args);
var connectionString = 
    builder.Configuration["ConnectionString:Value"] ?? 
    throw new InvalidOperationException("Connection string 'IdentityContextConnection' not found.");

builder.Services.AddDbContext<MilkTeaStoreTwoContext>(options => options.UseSqlServer(connectionString));

builder.Services.AddDefaultIdentity<ApplicationUser>(options => 
    {
        options.SignIn.RequireConfirmedAccount = true;
        options.SignIn.RequireConfirmedEmail = true;
        options.Password.RequiredUniqueChars = 0;
        options.Password.RequireUppercase = false;
        
    }).AddRoles<IdentityRole>().
    AddEntityFrameworkStores<MilkTeaStoreTwoContext>();

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddTransient<IEmailSender, EmailSender>();
builder.Services.AddControllers().AddNewtonsoftJson(options =>
    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
);

try
{
    var email = builder.Configuration["MailSettings:Email"];
    var password = builder.Configuration["MailSettings:Password"];
    var port = builder.Configuration["MailSettings:Port"];
    var ssl = builder.Configuration["MailSettings:EnableSSL"];
    var displayName = builder.Configuration["MailSettings:DisplayName"];
    var host = builder.Configuration["MailSettings:Host"];
    builder.Services.Configure<EmailSettings>(options =>
    {
        options.Email = email!;
        options.Password = password!;
        options.Port = Convert.ToInt32(port);
        options.EnableSsl = Convert.ToBoolean(ssl);
        options.DisplayName = displayName!;
        options.Host = host!;
    });
}
catch (Exception e)
{
    throw new Exception(e.Message);
}


var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var service = scope.ServiceProvider;

    await SeedData.SeedRoles(service);
}

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();
app.MapRazorPages();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
