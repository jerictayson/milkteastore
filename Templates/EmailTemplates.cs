using MilkTeaStoreApp.Models;

namespace MilkTeaStoreApp.Templates;

public static class EmailTemplates
{
    public static string SendOrderToCustomer(string name, Order order)
    {

        return @"
            <div style='width: 100%; height: 100%; background-color: #f2f2f2; padding: 50px 0;'>
                <div style='width: 80%; margin: 0 auto; background-color: #fff; padding: 50px 0;'>
                    <div style='width: 80%; margin: 0 auto;'>
                        <div style='width: 100%; height: 100px; background-color: #f2f2f2;'>
                            <h1 style='text-align: center; padding-top: 30px;'>Milk Tea Store</h1>
                        </div>
                        <div style='width: 100%; height: 100%; padding: 50px 0;'>
                            <h2 style='text-align: center; padding-bottom: 30px;'>Hello " + name + @"</h2>
                            <p style='text-align: center; padding-bottom: 30px;'>Your order has been placed successfully</p>
                            <p style='text-align: center; padding-bottom: 30px;'>Order Id: " + order.OrderId + @"</p>
                            <p style='text-align: center; padding-bottom: 30px;'>Order Date: " + order.CreatedDate!.Value.ToString("yy-MMM-dd ddd") + @"</p>
                            <p style='text-align: center; padding-bottom: 30px;'>Total Price: " + order.OrderDetails.Sum(total => total.TotalPrice) + @"</p>
                            <p style='text-align: center; padding-bottom: 30px;'>Status: " + order.Status + @"</p>
                            <p style='text-align: center; padding-bottom: 30px;'>Thank you for choosing us</p>
                        </div>
                    </div>
                </div>
            </div>
        ";
    }
}