
$(document).ready(function(){
    
    
    loadCartCount();
});

function loadCartCount(){
    
    $.get('/Cart/GetCartCount/', function(data){
        try{
            
            $('#cart-count').html(data);
        }catch (e){
            
        }
    }).fail(function (xhr, textStatus, errorThrown) {
        console.clear();
    });
    
}

function addToCart(id){
    
    console.log(id)
    $.ajax({
        url: '/Cart/AddToCart',
        type: 'POST',
        data: {id: id},
        success: function(data){
            
            loadCartCount();
        },
        statusCode: {
            401: function(){
                window.location.href = '/Identity/Account/Login';
            }
        }
    })
}