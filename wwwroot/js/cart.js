
const cart = $('#carts');
const items = new Set();
$(document).ready(function () {
    
    setEventListeners();   
});

function setItemQuantity(id){
    
    let quantity = parseInt($('#quantity-'+id).val());
    let isExist = false;

    for (let item of items) {
        if(item.cartId === id){
            item.quantity = quantity;
            isExist = true;
            break;
        }
    }
    if(!isExist)
        items.add({cartId: id, quantity: quantity});
    console.log(items);
}


function processItems(){
    
    $('#carts .form-check-input:checked').each(function () {

        let id = $(this).val();
        setItemQuantity(id);

    });
    return items;
}

function setEventListeners() {

    cart.on('click',  '#delete-btn', function () {
        console.log('delete');
        let count = processItems().size;
        if(count !== 0) {

            $('#deleteModal').modal('show');

        }
    });
    
    cart.on('click', '#checkout', function () {
        
        let itemIds = processItems().size;
        if(itemIds !== 0){
            
            let carts = [];
            for (let item of items) {
                carts.push({cartId: item.cartId, quantity: item.quantity});
            }
            $.ajax({
                url: '/Cart/Checkout/',
                data: JSON.stringify(carts),
                type: 'POST',
                contentType: 'application/json',
                success: function (res) {

                    window.location.href = '/Cart/Checkout/';
                },
                error: function (res, status) {
                    
                    console.log(res);
                    console.log(status);
                    alert('Submit failed!');
                }
            });
            
        }else {
            alert('Please select at least one item');
        }

    });
    
    $('#delete-confirm').click(function() {
        let isSuccess = true;
        //return all ids of checked items
        let ids = processItems();

        $.ajax({
            url: '/Cart/RemoveFromCart/',
            data: {carts: ids},
            type: 'DELETE',
            success: function (res) {

                console.log('Success!');
            },
            error: function () {
                isSuccess = false;
                alert('Submit failed!');
            }
        });

    });



}

function fetchCart(){
    
    $.get('Cart/FetchUserCarts/', function (data) {
        
        if(data.length === 0){
            let placeholder = '<div class="text-center">No items in cart</div>'; 
            $('#carts').html(placeholder);
        }else {
            
            cart.html('');
            let container = document.createElement('div');
            container.classList.add('d-flex');
            container.classList.add('justify-content-end');
            container.classList.add('mb-2');
            
            let deleteBtn = document.createElement('button');
            deleteBtn.classList.add('btn');
            deleteBtn.classList.add('btn-outline-danger');
            deleteBtn.id = 'delete-btn';
            
            let btnIcon = document.createElement('i');
            btnIcon.classList.add('fa-solid');
            btnIcon.classList.add('fa-trash');
            deleteBtn.append(btnIcon);
            container.append(deleteBtn);
            
            let cartcontainer = document.createElement('div');
            cartcontainer.classList.add('align-items-center');
            cartcontainer.classList.add('mt-1');
            for(let i = 0; i<data.length; i++){
                
                console.log(data[i]);
                cartcontainer.append(createWindowCart(data[i]));
            }
            cart.append(container);
            cart.append(cartcontainer);
            
            let divCheckout = document.createElement('div');
            divCheckout.classList.add('d-flex');
            divCheckout.classList.add('justify-content-end');
            divCheckout.classList.add('mt-3');
            
            let btnCheckout = document.createElement('button');
            btnCheckout.classList.add('btn');
            btnCheckout.classList.add('btn-primary');
            btnCheckout.classList.add('fw-bolder');
            btnCheckout.classList.add('p-2');
            btnCheckout.classList.add('pt-3');
            btnCheckout.classList.add('pb-3');
            btnCheckout.innerText = 'Proceed to Checkout';
            divCheckout.append(btnCheckout);
            cart.append(divCheckout);
            
        }
        
        function createWindowCart(data){
            
            let divElement = document.createElement('div');
            divElement.style.backgroundColor = '#a89ca2';
            divElement.classList.add('p-2');
            divElement.classList.add('mb-2');
            divElement.classList.add('rounded-1');
            divElement.classList.add('d-flex');
            divElement.classList.add('justify-content-between');
            
            let detail = document.createElement('div');
            detail.classList.add('d-flex');
            detail.classList.add('gap-2');
            
            let img = document.createElement('img');
            img.src = data.product.imagePath;
            img.classList.add('roudend-2');
            img.style.width = '100px';
            
            detail.append(img);
            
            let info = document.createElement('div');

            let lblName = document.createElement('p');
            lblName.innerText = 'Item name: ';
            lblName.classList.add('mb-0');
            let name = document.createElement('p');
            name.innerText = data.product.productName;
            
            let lblprice = document.createElement('p');
            lblprice.innerText = 'Price: ';
            lblprice.classList.add('mb-0');
            let price = document.createElement('p');
            price.innerText = data.product.price;
            
            info.append(lblName);
            info.append(name);
            info.append(lblprice);
            info.append(price);
            
            detail.append(info);
            
            let quantityInfo = document.createElement('div');

            let lblQuantity = document.createElement('p');
            lblQuantity.innerText = 'Quantity: ';
            lblQuantity.classList.add('mb-0');
            let quantity = document.createElement('p');
            quantity.innerText = data.quantity;
            
            quantityInfo.append(lblQuantity);
            quantityInfo.append(quantity);
            
            divElement.append(detail);
            divElement.append(quantityInfo);

            let checkboxDiv = document.createElement('div');
            checkboxDiv.classList.add('justify-content-end');
            checkboxDiv.classList.add('d-flex');
            checkboxDiv.classList.add('align-items-center');
            let input = document.createElement('input');
            input.type = 'checkbox';
            input.classList.add('form-check-input');
            input.classList.add('me-3');
            input.value = data.product.productId;
            
            checkboxDiv.append(input);
            divElement.append(checkboxDiv);
            
            return divElement;
        }
    });
}