$(document).ready(() => {

    let productTable = $('#productTable').DataTable();
    
    $.ajax('GetAll/', {
        method: 'GET',
        dataType: 'json',
        success: (res) => {
            for(let key in res){
                for (let i = 0; i < res[key].length; i++) {
                    productTable.row.add([
                        res[key][i]["id"],
                        res[key][i]["name"],
                        res[key][i]['price'],
                        `<button class="btn btn-warning" onclick="editProduct(${res[key][i].id})">Edit</button>`
                    ]).draw(false);
                }
            }
        },
        
    });
})