let navButtons;
let table;
let orderDetails;
let cards;
let ordersDiv;
let title;

$(document).ready(function() {
    table = new DataTable('#myTable');
    orderDetails = $('#order-details');
    ordersDiv = $('#orders-div');
    cards = $('#cards');
    title = $('#title');
    showCards();
    fetchOrders();
    navButtons = $('.nav-button');
    clearActive();
    navButtons.eq(0).addClass('active');

    navButtons.click(function() {
        clearActive();
        $(this).addClass('active');

        switch ($(this).attr('value')) {
            case 'overview':
                clearAllActive();
                showCards();
                fetchOrders();
                break;
            case 'manage-order':
                clearAllActive();
                showOrderTable();
                break;
            case 'manage-user':
                break;
            default:
                break;
        }
    });
});

function clearAllActive(){

    cards.hide();
    ordersDiv.hide();
    orderDetails.html('');
    orderDetails.hide();
}

function showOrderTable(){
    clearAllActive();
    table.clear().draw(false);
    $.ajax({

        url: 'Dashboard/FetchOrders/',
        type: 'post',
        success: function(data, status) {
            for (let value in data) {
                $.ajax({
                    url: `Dashboard/FetchCustomerName/?accountId=${data[value]["accountId"]}`,
                    type: 'get',
                    success: function (res){
                        const id = data[value]["orderId"]
                        table.row.add([
                            id,
                            formatDate(data[value]["createdDate"]),
                            data[value]["status"],
                            `${res.firstName} ${res.lastName}`,
                            `<button class="btn btn-primary" onclick="showDetails(${id}, '${res.id}', '${data[value]['status']}')">Show Details</button>`
                        ]).draw(false);
                    }
                });

            }

        }
    })

    title.html('Orders');
    ordersDiv.show();
}

function showDetails(id, userId, status){
    let totalQuantity = 0, totalPrice = 0;
    $.ajax({

        url: `Dashboard/Details/${id}`,
        type: 'get',
        success: function (data){
            let div = $('<div></div>');
            div.addClass('col-8');
            for(let i in data){
                div.append(createCard(data[i].product.imagePath, data[i].product.productName, data[i].product.price,
                    data[i].quantity, data[i].product.price * data[i].quantity));
                totalQuantity += data[i].quantity;
                totalPrice += data[i].product.price * data[i].quantity;
            }

            orderDetails.append(div);
            let aside = $('<div></div>');
            aside.addClass('col-4');
            createSummary(totalQuantity, totalPrice, userId, status, id);
            ordersDiv.hide();
        }
    })
}

function createCard(img, productName, price, quantity, total){

    let div = $('<div></div>');
    div.addClass('row');
    div.addClass('col-md-12');
    let imageDiv = $('<div></div>');
    imageDiv.addClass('col-md-2');
    imageDiv.addClass('d-flex');
    imageDiv.addClass('align-items-center');
    let image = $('<img alt=""/>');
    image.addClass('img');
    image.attr('src', img);
    image.attr('width', '135px');
    imageDiv.append(image);
    let infoDiv = $('<div></div>');
    infoDiv.addClass('col-md-10');
    let productNameH4 = $('<h4></h4>');
    productNameH4.html(productName);
    let priceP = $('<p></p>');
    priceP.html(`Price: ${price}`);
    let quantityP = $('<p></p>');
    quantityP.html(`Quantity: ${quantity}`);
    let totalP = $('<p></p>');
    totalP.html(`Total: ${total}`);
    infoDiv.append(productNameH4);
    infoDiv.append(priceP);
    infoDiv.append(quantityP);
    infoDiv.append(totalP);
    div.append(imageDiv);
    div.append(infoDiv);

    return div;
}

function clearActive() {
    $('.nav-button').removeClass('active');
}

function processOrder(id, accountId){
    
    $.ajax({
        
        url: `/Dashboard/FullFillOrder/id`,
        type: 'post',
        data: {
            orderId: id,
            accountId: accountId,
            status: 'Order on its Way'
        },
        success: function (data, status){
            
            if (status === 'success'){
                alert('Order has been fulfilled');
                location.reload();
            }else {
                alert('Error');
            }
        }
    })
}

function createSummary(quantity, total, id, status, orderDetailsId){

    let card;
    $.ajax({
        success: function (data){

            let div = $('<div></div>');
            div.addClass('col-4');
            div.addClass('p-3');

            let mainDiv = $('<div></div>');
            mainDiv.addClass('row');
            mainDiv.addClass('justify-content-center');

            let row1 = $('<div></div>');
            row1.addClass('col-12');
            row1.addClass('row');

            let headerH1 = $('<h1></h1>');
            headerH1.addClass('fw-bolder');
            headerH1.addClass('mb-5');
            headerH1.addClass('col-12');
            headerH1.addClass('text-center');
            headerH1.html('Order Details');

            row1.append(headerH1);

            let quantityDiv = $('<div></div>');
            quantityDiv.addClass('row');
            quantityDiv.addClass('col-12');
            quantityDiv.addClass('justify-content-between');
            let quantityP = $('<p></p>');
            quantityP.addClass('col');
            quantityP.html('Quantity');
            let quantityP2 = $('<p></p>');
            quantityP2.html(quantity);
            quantityDiv.append(quantityP);
            quantityDiv.append(quantityP2);

            row1.append(quantityDiv);

            let totalDiv = $('<div></div>');
            totalDiv.addClass('row');
            totalDiv.addClass('col-12');
            totalDiv.addClass('justify-content-between');
            let totalP = $('<p></p>');
            totalP.addClass('col');
            totalP.html('Total');
            let totalP2 = $('<p></p>');
            totalP2.html(total);
            totalDiv.append(totalP);
            totalDiv.append(totalP2);

            row1.append(totalDiv);

            mainDiv.append(row1);
            let row2 = $('<div></div>');
            row2.addClass('row');
            row2.addClass('col-12');

            let row2Container = $('<div></div>');
            row2Container.addClass('col-12');
            let h5 = $('<h5></h5>');
            h5.addClass('fw-bolder');
            h5.html('Address Info');
            row2Container.append(h5);

            let row2MainContainer = $('<div></div>');
            row2MainContainer.addClass('row');
            let accNameCon = $('<div></div>');
            accNameCon.addClass('row');
            accNameCon.addClass('col-12');
            accNameCon.addClass('justify-content-between');
            let p1 = $('<p></p>');
            p1.addClass('col-5');
            p1.html('Account Name');
            let p2 = $('<p></p>');
            p2.addClass('col-7');
            p2.css('text-align', 'end');
            p2.addClass('text-wrap');
            p2.addClass('p-0');
            p2.html(data.firstName + ' ' + data.lastName);
            accNameCon.append(p1);
            accNameCon.append(p2);
            row2MainContainer.append(accNameCon);

            let addressCon = $('<div></div>');
            addressCon.addClass('row');
            addressCon.addClass('col-12');
            addressCon.addClass('justify-content-between');
            let p3 = $('<p></p>');
            p3.addClass('col-5');
            p3.html('Address');
            let p4 = $('<p></p>');
            p4.addClass('col-7');
            p4.addClass('p-0');
            p4.css('text-align', 'end');
            p4.addClass('text-wrap');
            p4.html(data.street + ' ' + data.province);
            addressCon.append(p3);
            addressCon.append(p4);
            row2MainContainer.append(addressCon);
            let phoneNumCon = $('<div></div>');
            phoneNumCon.addClass('row');
            phoneNumCon.addClass('col-12');
            phoneNumCon.addClass('justify-content-between');
            let p5 = $('<p></p>');
            p5.addClass('col-5');
            p5.html('Phone Number');
            let p6 = $('<p></p>');
            p6.addClass('col-7');
            p6.addClass('p-0');
            p6.css('text-align', 'end');
            p6.addClass('text-wrap');
            p6.html(data.phoneNumber);
            phoneNumCon.append(p5);
            phoneNumCon.append(p6);

            let buttonCon = $('<div></div>');
            buttonCon.addClass('row');
            buttonCon.addClass('col-12');
            buttonCon.addClass('justify-content-between');
            let button = $('<button></button>');
            if (status === "Pending"){

                button.addClass('btn');
                button.addClass('btn-primary');
                button.addClass('col-12');
                button.html('Fulfill Order');
                button.click(function (){
                    
                    processOrder(orderDetailsId, data.id);
                });
            }else {
                button.addClass('btn');
                button.addClass('btn-primary');
                button.addClass('col-12');
                button.html('Mark order as Received');
                if(status === 'Received')
                    button.attr('disabled', 'disabled');
                button.click(function () {
                  
                    let status = confirm('Are you sure you want to fulfill this order?');
                    if (status){
                        $.ajax({
                            url: `/Dashboard/ReceivedOrder/${orderDetailsId}`,
                            type: 'post',
                            success: function (data, status){
                                if (status === 'success'){
                                    alert('Order has been fulfilled');
                                    location.reload();
                                }else {
                                    alert('Error');
                                }
                            }
                        })
                    }else {
                        alert('Order has not been fulfilled');
                    }
                });

            }
            let cancelButton = $('<button></button>');
            cancelButton.addClass('btn');
            cancelButton.addClass('btn-danger');
            cancelButton.addClass('col-12');
            if(status === 'Received'){
                cancelButton.attr('disabled', 'disabled');
            }
            cancelButton.click(function (){
                let status = confirm('Are you sure you want to cancel this order?');
                if (status){
                    window.location.reload();    
                }
            });
            cancelButton.html('Cancel Order');
            buttonCon.append(button);
            buttonCon.append(cancelButton);
            row2MainContainer.append(phoneNumCon);
            row2MainContainer.append(buttonCon);
            row2Container.append(row2MainContainer);
            row2.append(row2Container);

            mainDiv.append(row2);
            div.append(mainDiv);
            card = div;
            orderDetails.append(card);
            orderDetails.show();
        },
        type: 'get',
        url: `Dashboard/FetchCustomerName/?accountId=${id}`
    });
}


function formatDate(date){
    let d = new Date(date);
    return `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`;
}

function showCards(){
    title.html('Overview');
    cards.show();
    ordersDiv.hide();

}

function fetchOrders(){
    $.ajax({
        url: '/Dashboard/FetchOrders/',
        type: 'GET',
        success: function(data) {
            $('#total-orders').html(data.length);
            let pendingOrders = data.filter(order => order.isFulfilled == false && order.status == 'Pending');
            $('#pending-orders').html(pendingOrders.length);
        },
        error: function() {
            return 0;
        }
    });
}
 