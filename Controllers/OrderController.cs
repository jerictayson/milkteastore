using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MilkTeaStoreApp.Areas.Identity.Data;
using MilkTeaStoreApp.Data;
using MilkTeaStoreApp.Models;

namespace MilkTeaStoreApp.Controllers;

[Authorize]
public class OrderController : Controller
{
    private readonly MilkTeaStoreTwoContext _context;
    private readonly SignInManager<ApplicationUser> _signInManager;

    public OrderController(MilkTeaStoreTwoContext context, SignInManager<ApplicationUser> signInManager)
    {
        _context = context;
        _signInManager = signInManager;
    }

    // GET: Order
    [Authorize(Roles = "Customer")]
    public async Task<IActionResult> Index()
    {
        var user = _signInManager.UserManager.GetUserAsync(User).Result;
        var result = await _context.Order
            .Include(p => p.OrderDetails)
            .Where(o => o.AccountId == user!.Id)
            .ToListAsync();

        return View(result);
    }

    // GET: Order/Details/5
    [Authorize(Roles = "Customer")]
    public async Task<IActionResult> Details(int? id)
    {
        var user = _signInManager.UserManager.GetUserAsync(User).Result;
        if (id == null || _context.Order == null) return NotFound();
        
        var acc = await _context.Account.FindAsync(user!.Id);
        if (acc == null) return NotFound();
        ViewData["user"] = acc;

        var order = await _context.OrderDetails
            .Include(o => o.Order)
            .Include(o => o.Product)
            .Where(o => o.OrderId == id && o.Order!.AccountId == user!.Id)
            .ToListAsync();

        if (!order.Any()) return NotFound();

        ViewData["Status"] = _context.Order.Find(id)?.Status == "Pending";

        return View(order);
    }
    
    [Authorize]
    public IActionResult FetchUserNotification()
    {
        var user = _signInManager.UserManager.GetUserAsync(User).Result;
        var result = _context.Notification.
            Where(n => 
                n.AccountId == user!.Id 
                && n.IsRead == false).ToList();
        return Json(result);
    }
    
    [Authorize]
    [HttpPost]
    public IActionResult ReadNotification(int? id)
    {
        if (id == null || _context.Notification == null) return NotFound();
        var notification = _context.Notification.Find(id);
        if (notification == null) return NotFound();
        notification.IsRead = true;
        _context.SaveChanges();
        return Json(notification);
    }

    // POST: Order/Create
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("OrderId,UserId,CreatedDate,IsFulfilled")] Order order)
    {
        if (ModelState.IsValid)
        {
            _context.Add(order);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        ViewData["UserId"] = new SelectList(_context.Set<UserAccount>(), "Id", "Id", order.AccountId);
        return View(order);
    }

    // GET: Order/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
        if (id == null || _context.Order == null) return NotFound();

        var order = await _context.Order.FindAsync(id);
        if (order == null) return NotFound();
        ViewData["UserId"] = new SelectList(_context.Set<UserAccount>(), "Id", "Id", order.AccountId);
        return View(order);
    }

    // POST: Order/Edit/5
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("OrderId,UserId,CreatedDate,IsFulfilled")] Order order)
    {
        if (id != order.OrderId) return NotFound();

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(order);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(order.OrderId))
                    return NotFound();
                throw;
            }

            return RedirectToAction(nameof(Index));
        }

        ViewData["UserId"] = new SelectList(_context.Set<UserAccount>(), "Id", "Id", order.AccountId);
        return View(order);
    }

    // GET: Order/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
        if (id == null || _context.Order == null) return NotFound();

        var order = await _context.Order
            .Include(o => o.Account)
            .FirstOrDefaultAsync(m => m.OrderId == id);
        if (order == null) return NotFound();

        return View(order);
    }

    [Authorize(Roles = "Employee")]
    public async Task<IActionResult> ManageOrders()
    {
        var orders = await _context.Order.Where(o => o.Status == "Pending" && o.IsFulfilled == false).ToListAsync();
        return View(orders);
    }

    // POST: Order/Delete/5
    [HttpPost]
    [ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        if (_context.Order == null) return Problem("Entity set 'MilkTeaStoreTwoContext.Orders'  is null.");
        var order = await _context.Order.FindAsync(id);
        if (order != null) _context.Order.Remove(order);

        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool OrderExists(int id)
    {
        return (_context.Order?.Any(e => e.OrderId == id)).GetValueOrDefault();
    }
}