using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MilkTeaStoreApp.Areas.Identity.Data;
using MilkTeaStoreApp.Data;
using MilkTeaStoreApp.Models;
using MilkTeaStoreApp.Templates;

namespace MilkTeaStoreApp.Controllers;

[Authorize(Roles = "Employee, Admin")]
public class DashboardController : Controller
{
    private readonly MilkTeaStoreTwoContext _context;
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly IEmailSender _emailSender;

    public DashboardController(MilkTeaStoreTwoContext context, SignInManager<ApplicationUser> signInManager, IEmailSender emailSender)
    {
        _context = context;
        _signInManager = signInManager;
        _emailSender = emailSender;
    }

    // GET
    public IActionResult Index()
    {
        return View();
    }

    //GET
    public async Task<IEnumerable<Order>> FetchOrders()
    {
        var orders = await _context.Order.Include(acc => acc.Account).ToListAsync();
        return orders;
    }

    public IActionResult FetchCustomerName(string accountId)
    {
        var user = _context.Account.Find(accountId);
        if (user == null) return NotFound();
        return Json(new
            { user.Id, user.FirstName, user.LastName, user.Street, user.Province, user.PhoneNumber });
    }

    [Authorize(Roles = "Admin")]
    [HttpPost]
    public IActionResult CreateNewAccount([FromForm] ApplicationUserWithRole ApplicationUser)
    {
        throw new NotImplementedException();
    }
    [Authorize(Roles = "Admin,Employee")]
    [HttpPost]
    public async Task<IActionResult> FullFillOrder([FromForm] int? orderId, string accountId, string status)
    {
        if (orderId == null)
            return BadRequest();
        var order = _context.Order.Include(od => od.OrderDetails).FirstOrDefault(order => order.OrderId == orderId);
        if (order == null)
            return NotFound();
        order.Status = status;
        order.AccountId = accountId;
        _context.Update(order);
        var userNotification = new UserNotification
        {
            AccountId = accountId,
            Message = $"Your order with id {orderId} has been {status}",
            IsRead = false
        };
        _context.Add(userNotification);
        var user = await _context.Users.FindAsync(accountId);
        await _emailSender.SendEmailAsync(user!.Email!, "Order Status", EmailTemplates.SendOrderToCustomer(user.Email!, order));
        await _context.SaveChangesAsync();
        return Ok();
    }

    public async Task<IActionResult> Details(int? id)
    {
        if (id == null || _context.Order == null) return NotFound();

        var order = await _context.OrderDetails
            .Include(o => o.Product)
            .Include(o => o.Order)
            .Where(o => o.OrderId == id)
            .ToListAsync();

        if (!order.Any()) return NotFound();

        return Ok(order);
    }
    
    [HttpPost]
    public async Task<IActionResult> ReceivedOrder(int id)
    {
        if (await _context.Order.FindAsync(id) == null) return NotFound();
        var order = await _context.Order.FindAsync(id);
        if (order == null) return NotFound();
        order.Status = "Received";
        _context.Update(order);
        await _context.SaveChangesAsync();

        return Ok();
    }

    public class ApplicationUserWithRole : ApplicationUser
    {
        public string Role { get; set; }
    }
}