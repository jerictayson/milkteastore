using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MilkTeaStoreApp.Areas.Identity.Data;
using MilkTeaStoreApp.Data;
using MilkTeaStoreApp.Models;
using Newtonsoft.Json;

namespace MilkTeaStoreApp.Controllers;

[Authorize]
public class CartController : Controller
{
    private readonly MilkTeaStoreTwoContext _context;
    private readonly SignInManager<ApplicationUser> _signInManager;

    public CartController(MilkTeaStoreTwoContext context, SignInManager<ApplicationUser> signInManager)
    {
        _context = context;
        _signInManager = signInManager;
    }

    [HttpPost]
    public IActionResult Checkout([FromBody] List<Cart> carts)
    {
        //store carts to cookie
        var cookieOptions = new CookieOptions
        {
            Expires = DateTime.Now.AddDays(1)
        };
        Response.Cookies.Append("carts", JsonConvert.SerializeObject(carts), cookieOptions);
        return Ok();
    }

    public IActionResult Checkout()
    {
        var user = _signInManager.UserManager.GetUserAsync(User);
        
        var userAcc = _context.Account.Find(user.Result!.Id);
        if (userAcc == null) return NotFound();
        ViewData["user"] = userAcc;
        
        var cookieCarts = JsonConvert.DeserializeObject<List<Cart>>(Request.Cookies["carts"]);
        var carts = new List<Cart>();
        foreach (var cart in cookieCarts)
        {
            var isCartExist = _context.Cart
                .Include(p => p.Product)
                .FirstOrDefault(c => c.CartId == cart.CartId && c.Archived == false);
            if (isCartExist != null)
                carts.Add(isCartExist);
            else
                carts.Add(cart);
        }

        return View(carts);
    }

    //GET
    public IActionResult Index()
    {
        return View(FetchUserCarts().Result);
    }

    [HttpGet]
    public async Task<IEnumerable<Cart>> FetchUserCarts()
    {
        var user = _signInManager.UserManager.GetUserAsync(User);
        var cart = await _context.Cart.Include(p => p.Product)
            .Where(c => c.AccountId == user.Result!.Id && c.Archived == false).ToListAsync();
        return cart;
    }

    [HttpGet]
    public async Task<IActionResult> GetCartCount()
    {
        var user = await _signInManager.UserManager.GetUserAsync(User);
        var cartCount = _context.Cart.Count(c => c.AccountId == user!.Id && c.Archived == false);
        return Ok(cartCount);
    }

    [HttpPost]
    public async Task<IActionResult> AddToCart(int id)
    {
        if (!_context.Product.Any(p => p.ProductId == id)) return NotFound();
        var user = await _signInManager.UserManager.GetUserAsync(User);

        var isCartExist =
            _context.Cart.FirstOrDefault(c => c.ProductId == id && c.AccountId == user!.Id && c.Archived == false);
        if (isCartExist != null)
        {
            isCartExist.Quantity += 1;
        }
        else
        {
            var cart = new Cart
            {
                AccountId = user!.Id,
                ProductId = id,
                Quantity = 1,
                CreatedDate = DateTime.Now,
                Archived = false
            };
            _context.Cart.Add(cart);
        }


        await _context.SaveChangesAsync();

        return Ok();
    }

    public IActionResult PlaceOrder(int? id)
    {
        if (id == null)
            return NotFound();
        var user = _signInManager.UserManager.GetUserAsync(User);
        var orderInfo = _context.Order
            .Include(orderDetail => orderDetail.OrderDetails).OrderBy(order => order.OrderId).FirstOrDefault(o =>
                o.OrderId == id
                && o.AccountId == user.Result!.Id);

        if (orderInfo == null)
            return NotFound();

        return View(orderInfo);
    }

    [HttpPost]
    public Task<IActionResult> PlaceOrder([FromBody] List<Cart> carts)
    {
        var user = _signInManager.UserManager.GetUserAsync(User).Result!.Id;

        var order = new Order
        {
            CreatedDate = DateTime.Today,
            IsFulfilled = false,
            AccountId = user
        };
        _context.Order.Add(order);
        _context.SaveChanges();


        var orderList = new List<OrderDetail>();
        foreach (var cart in carts)
        {
            var total = _context.Product.FirstOrDefault(p => p.ProductId == cart.ProductId)!.Price * cart.Quantity;
            orderList.Add(
                new OrderDetail
                {
                    OrderId = order.OrderId,
                    ProductId = cart.ProductId,
                    Quantity = cart.Quantity,
                    TotalPrice = total
                });
            var cartToRemove = _context.Cart.FirstOrDefault(c => c.CartId == cart.CartId);
            if (cartToRemove != null) cartToRemove.Archived = true;
            _context.SaveChanges();
        }

        order.OrderDetails = orderList;
        _context.Update(order);
        _context.SaveChanges();
        return Task.FromResult<IActionResult>(Ok(new { orderId = order.OrderId }));
    }

    [HttpDelete]
    public async Task<IActionResult> RemoveFromCart([FromForm] int[] ids)
    {
        var user = await _signInManager.UserManager.GetUserAsync(User);

        foreach (var id in ids)
        {
            var cart = _context.Cart.FirstOrDefault(c => c.ProductId == id && c.AccountId == user!.Id);
            if (cart == null) return NotFound();
            _context.Cart.Remove(cart);
            await _context.SaveChangesAsync();
        }

        return Ok();
    }
}