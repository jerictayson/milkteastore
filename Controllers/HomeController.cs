﻿using System.Diagnostics;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MilkTeaStoreApp.Areas.Identity.Data;
using MilkTeaStoreApp.Data;
using MilkTeaStoreApp.Models;

namespace MilkTeaStoreApp.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly MilkTeaStoreTwoContext _context;
    private readonly SignInManager<ApplicationUser> _signInManager;
    public HomeController(ILogger<HomeController> logger, MilkTeaStoreTwoContext context, SignInManager<ApplicationUser> signInManager)
    {
        _logger = logger;
        _context = context;
        _signInManager = signInManager;
    }

    public async Task<IActionResult> Index()
    {
        var user = _signInManager.UserManager.GetUserAsync(User).Result;
        if (user != null)
        {
            var userRole = await _signInManager.UserManager.GetRolesAsync((await _signInManager.UserManager.FindByEmailAsync(user.Email!))!);
            if (userRole.Contains("Admin") || userRole.Contains("Employee"))
            {
                ViewData["Dashboard"] = "active";
            }
        }
        var products = await _context.Product.OrderByDescending(x => x.ProductId).ToListAsync();
        products = products.Take(8).ToList();
        ViewData["Home"] = "active";
        return View(products);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}