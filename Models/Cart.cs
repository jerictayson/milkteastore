﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MilkTeaStoreApp.Models;

[Table("Cart")]
public class Cart
{
    public int CartId { get; set; }
    public DateTime? CreatedDate { get; set; }
    public int Quantity { get; set; }
    public bool? Archived { get; set; }
    public string AccountId { get; set; } = null!;
    public UserAccount Account { get; set; } = null!;
    public int ProductId { get; set; }
    public Product Product { get; set; } = null!;

}