﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MilkTeaStoreApp.Models;

[Table("Order")]
public class Order
{
    public int OrderId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public bool? IsFulfilled { get; set; }

    public string Status { get; set; } = "Pending";

    public bool Archived { get; set; } = false;

    public string AccountId {get; set;}
    public UserAccount Account { get; set; } = null!;

    public ICollection<OrderDetail> OrderDetails { get; set; } = new List<OrderDetail>();
}