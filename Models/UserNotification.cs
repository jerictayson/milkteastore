using MilkTeaStoreApp.Areas.Identity.Data;

namespace MilkTeaStoreApp.Models;

public class UserNotification
{
    public int Id { get; set; }
    public string Message { get; set; } = null!;
    public bool IsRead { get; set; } = false;
    public string AccountId {get; set;}
    public UserAccount Account { get; set; } = null!;
}