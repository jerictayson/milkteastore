using Microsoft.AspNetCore.Identity;
using MilkTeaStoreApp.Areas.Identity.Data;
using MilkTeaStoreApp.Models;

public class UserAccount {

    public string Id {get; set;}
    [PersonalData] public string FirstName { get; set; } = null!;
    [PersonalData] public string LastName { get; set; } = null!;
    [PersonalData] public string Gender { get; set; } = null!;
    [PersonalData] public string Province { get; set; } = null!;
    [PersonalData] public string Street { get; set; } = null!;
    [PersonalData] public string? PhoneNumber { get; set;}
    public ApplicationUser Account {get; set;} = null!;
    public ICollection<Cart> Carts {get; set;} = null!;
    public ICollection<Order> Orders {get; set;} = null!;
    public ICollection<UserNotification> Notifications {get; set;} = null!;
}