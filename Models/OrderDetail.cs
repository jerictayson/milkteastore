﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MilkTeaStoreApp.Models;

public class OrderDetail
{
    public int ProductId { get; set; }
    public int OrderId { get; set; }
    public Product Product { get; set; } = null!;
    public Order Order { get; set; } = null!;
    public int Quantity { get; set; }
    public double? TotalPrice { get; set; }
}