using Microsoft.AspNetCore.Identity;

namespace MilkTeaStoreApp.Areas.Identity.Data;

// Add profile data for application users by adding properties to the UserAccount class
public class ApplicationUser : IdentityUser
{
    public UserAccount Account {get; set;}
}