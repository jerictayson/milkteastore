﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MilkTeaStoreApp.Models;

public class Product
{
    public int ProductId { get; set; }

    public string ProductName { get; set; } = null!;

    public double Price { get; set; }

    public string? Description { get; set; }

    public string? ImagePath { get; set; }

    public ICollection<OrderDetail> OrderDetails { get; set; } = null!;
    public ICollection<Cart> Carts {get; set;} = null!;
}