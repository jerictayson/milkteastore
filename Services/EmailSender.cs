using System.Net;
using System.Net.Mail;
using System.Text;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using MilkTeaStoreApp.Config;

namespace MilkTeaStoreApp.Services;

public class EmailSender : IEmailSender
{
    private readonly EmailSettings _emailSettings;
    private readonly ILogger _logger;

    public EmailSender(IOptions<EmailSettings> emailSettings, ILogger<EmailSender> logger)
    {
        _emailSettings = emailSettings.Value;
        _logger = logger;
    }

    public async Task SendEmailAsync(string toEmail, string subject, string message)
    {
        await Execute(subject, message, toEmail);
    }

    private Task Execute(string subject, string message, string toEmail)
    {
        using (var smtp = new SmtpClient())
        {
            smtp.Credentials = new NetworkCredential(_emailSettings.Email, _emailSettings.Password);
            smtp.Port = _emailSettings.Port;
            smtp.Host = _emailSettings.Host;
            smtp.EnableSsl = true;
            var msg = new MailMessage();
            msg.To.Add(new MailAddress(toEmail));
            msg.From = new MailAddress(_emailSettings.Email, _emailSettings.DisplayName, Encoding.UTF8);
            msg.Subject = subject;
            msg.Body = message;
            msg.IsBodyHtml = true;
            smtp.Send(msg);
        }

        _logger.LogInformation($"Email sent to {toEmail}!");
        return Task.CompletedTask;
    }
}