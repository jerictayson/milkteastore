﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MilkTeaStoreApp.Areas.Identity.Data;
using MilkTeaStoreApp.Models;

namespace MilkTeaStoreApp.Data;

public partial class MilkTeaStoreTwoContext : IdentityDbContext<ApplicationUser>
{
    public MilkTeaStoreTwoContext()
    {
    }

    public MilkTeaStoreTwoContext(DbContextOptions<MilkTeaStoreTwoContext> options)
        : base(options)
    {
    }
    public DbSet<UserAccount> Account { get; set; }

    public virtual DbSet<Cart> Cart { get; set; }
    public virtual DbSet<UserNotification> Notification { get; set; }
    public virtual DbSet<Order> Order { get; set; }
    public virtual DbSet<OrderDetail> OrderDetails { get; set; }
    public virtual DbSet<Product> Product { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {

        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<Cart>(
            c => c.HasOne(p => p.Product).
            WithMany(od => od.Carts).
            HasForeignKey(fk => fk.ProductId)
        );

        modelBuilder.Entity<Cart>(
            c => c.HasKey(k => k.CartId)
        );

        modelBuilder.Entity<Cart>(
            c => c.HasOne(acc => acc.Account).
            WithMany(c => c.Carts).
            HasForeignKey(acc => acc.AccountId)
        );

        modelBuilder.Entity<Order>(
            c => c.HasOne(acc => acc.Account).
            WithMany(c => c.Orders).
            HasForeignKey(acc => acc.AccountId)
        );

        modelBuilder.Entity<Order>(
            o => o.
            HasKey(pk => pk.OrderId)
        );
        
        modelBuilder.Entity<Product>(
            o => o.
            HasKey(pk => pk.ProductId)
            );

        modelBuilder.Entity<Order>(
            o => o.
            HasOne(acc => acc.Account).
            WithMany(c => c.Orders).
            HasForeignKey(acc => acc.AccountId)
        );

        modelBuilder.Entity<UserNotification>(
            notif => notif.
            HasOne(acc => acc.Account).
            WithMany(c => c.Notifications).
            HasForeignKey(fk => fk.AccountId)
        );
        modelBuilder.Entity<UserAccount>(
            acc => acc.HasKey(pk => pk.Id)
            );
        modelBuilder.Entity<UserAccount>(
            acc => acc.
            HasOne(uac => uac.Account).
            WithOne(ap => ap.Account).
            HasForeignKey<UserAccount>(fk => fk.Id)
        );
        modelBuilder.Entity<OrderDetail>(
            od => od.
                HasOne(o => o.Order).
                WithMany(order => order.OrderDetails).
                HasForeignKey(fk => fk.OrderId)
            );
                        
        modelBuilder.Entity<OrderDetail>(
            od => od.
                HasOne(o => o.Product).
                WithMany(product => product.OrderDetails).
                HasForeignKey(fk => fk.ProductId)
            );
        
        modelBuilder.Entity<OrderDetail>(
            od => od.
                HasKey(pk => new {pk.OrderId, pk.ProductId})
            );

        modelBuilder.Entity<ApplicationUser>().ToTable("Credential");
        modelBuilder.Entity<IdentityRole>().ToTable("Role");
        modelBuilder.Entity<IdentityUserRole<string>>().ToTable("UserRole");
        modelBuilder.Entity<IdentityUserClaim<string>>().ToTable("UserClaim");
        modelBuilder.Entity<IdentityUserLogin<string>>().ToTable("UserLogin");
        modelBuilder.Entity<IdentityRoleClaim<string>>().ToTable("RoleClaim");
        modelBuilder.Entity<IdentityUserToken<string>>().ToTable("UserToken");
        
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}